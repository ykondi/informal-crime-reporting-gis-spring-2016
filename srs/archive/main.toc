\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{6}{chapter.1}
\contentsline {section}{\numberline {1.1}Purpose}{6}{section.1.1}
\contentsline {section}{\numberline {1.2}Document Conventions}{6}{section.1.2}
\contentsline {section}{\numberline {1.3}Intended Audience}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Project Scope}{6}{section.1.4}
\contentsline {chapter}{\numberline {2}Overall Description}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Product Perspective}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Product Functions}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}User Classes and Characteristics}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}Operating Environment}{7}{section.2.4}
\contentsline {section}{\numberline {2.5}Design and Implementation Constraints}{7}{section.2.5}
\contentsline {section}{\numberline {2.6}User Documentation}{8}{section.2.6}
\contentsline {section}{\numberline {2.7}Assumptions and Dependencies}{8}{section.2.7}
\contentsline {chapter}{\numberline {3}External Interface Requirements}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}User Interfaces}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Hardware Interfaces}{9}{section.3.2}
\contentsline {section}{\numberline {3.3}Software Interfaces}{9}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}For data storage}{9}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Mapping Tool}{9}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Communications Interfaces}{10}{section.3.4}
\contentsline {chapter}{\numberline {4}System Features}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Authentication}{11}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Description and Priority}{11}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Functional Requirements}{11}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Map Visualization}{11}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Description and Priority}{11}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Functional Requirements}{11}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Data Management}{12}{section.4.3}
\contentsline {section}{\numberline {4.4}Customizing the map}{12}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Description and Priority}{12}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Functional Requirements}{12}{subsection.4.4.2}
\contentsline {section}{\numberline {4.5}Recording a crime}{13}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Description and Priorities}{13}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Functional Requirements}{13}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Additional features for authenticated users}{13}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Description and Priority}{13}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Functional Requirements}{14}{subsection.4.6.2}
\contentsline {chapter}{\numberline {5}Other Nonfunctional Requirements}{15}{chapter.5}
\contentsline {section}{\numberline {5.1}Performance Requirements}{15}{section.5.1}
\contentsline {section}{\numberline {5.2}Security Requirements}{15}{section.5.2}
\contentsline {section}{\numberline {5.3}Software Quality Attributes}{15}{section.5.3}
\contentsline {section}{\numberline {5.4}Business Rules}{15}{section.5.4}
